FROM ubuntu
LABEL maintainer="Michael Englert <michi.eng@gmail.com>"

ENV APPD_PLATFORM="/opt/appdynamics/platform"

ADD authorized_keys.pub /root/.ssh/authorized_keys
ADD start.sh ${APPD_PLATFORM}/controller/

RUN apt-get update \
    && apt-get install --fix-missing -q -y libaio1 libnuma1 lsof tzdata openssh-server \
    && echo "ulimit -n 65535" >> /etc/profile \
    && echo "ulimit -u 8192" >> /etc/profile \
    && echo "vm.swappiness = 10" >> /etc/sysctl.conf

EXPOSE 8090 8181

CMD [ "/bin/bash", "-c", "${APPD_PLATFORM}/controller/start.sh" ]