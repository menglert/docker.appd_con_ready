#!/bin/bash

/usr/sbin/service ssh start

find $APPD_PLATFORM/ -name *.log -exec /bin/bash -c "rm -rf {}" \;
find $APPD_PLATFORM/ -name *.lock -exec /bin/bash -c "rm -rf {}" \;
find $APPD_PLATFORM/ -name *.lck -exec /bin/bash -c "rm -rf {}" \;
find $APPD_PLATFORM/ -name *.pid -exec /bin/bash -c "rm -rf {}" \;
find $APPD_PLATFORM/ -name *.id -exec /bin/bash -c "rm -rf {}" \;

tail -f /dev/null